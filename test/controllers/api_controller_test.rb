require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  test "should get test" do
    get :test
    assert_response :success
  end

  test "should get getProcesses" do
    get :getProcesses
    assert_response :success
  end

end
