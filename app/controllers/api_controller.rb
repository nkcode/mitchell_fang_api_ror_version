class ApiController < ApplicationController

  def home
    render text: "api - rails version"
  end

  def test
    render text: RwProcess.all.to_json
  end

  def getProcesses
    render text: RwProcess.all.to_json
  end

  def getLotProcesses
    rec = RwLot.where(process_id: params[:process_id])
    if params[:lot_type]
      rec.where!(lot_type: params[:lot_type])
    end
    render text: rec.all.to_json
  end

  def updateLot
      if params[:lmLotId] && params[:lmLotId] != "undefined" && params[:lmLotId] != '-1'
        lot = RwLot.find(params[:lmLotId])
        lot.update_attributes lot_name: params[:lmLotName], invoice_no: params[:lmInvoiceNo], input_1: params[:lmInput1], input_2: params[:lmInput2], input_3: params[:lmInput3], lot_type: params[:lmLotType]
      else
        lot = RwLot.create process_id: params[:lmProcessId], lot_name: params[:lmLotName], invoice_no: params[:lmInvoiceNo], input_1: params[:lmInput1], input_2: params[:lmInput2], input_3: params[:lmInput3], lot_type: params[:lmLotType]
      end

      if !params[:lmContainerName].blank?
        container = RwContainer.create container_name: params[:lmContainerName], container_count: params[:lmContainerCount]
        RwLotContainer.create lot_id: lot.id, container_id: container.id
        ret = {"insertId"=>lot.id}
        render text: ret.to_json
        return
      end
      render text: lot.to_json
  end

  def getLotContainers
    containers = RwLotContainer.where("lot_id=?",params[:lot_id]).all.collect { |r| r.container }
    render text: containers.to_json
  end

  def updateContainer
    lot = RwLot.find(params[:lot_id])
    container = lot.containers.create container_name: params[:containerNo], container_count: params[:containerCount]
    render text: container.to_json
  end

  #this version of stored functions call works for current front end
  #node version returns some additional service data from stored functions, but that data seems not used
  def addSN
    ActiveRecord::Base.transaction do
      begin
        container = RwContainer.find(params[:container_id])
        unit = container.units.create serial_number: params[:SN]
        unit.statuses.create status_code: params[:status_code]    
        render text: [[{"@new_id"=>unit.unit_id}]].to_json
      rescue
        render text: [].to_json
        raise ActiveRecord::Rollback
      end
    end
  end

  def delSN
    ActiveRecord::Base.transaction do
      begin
        unit = RwUnit.where(serial_number: params[:SN]).first
        RwContainerUnit.where(unit_id: unit.unit_id).delete_all
        RwUnitStatus.where(unit_id: unit.unit_id).delete_all
        unit.destroy 
        render text: [[{"new_id"=>unit.unit_id}]].to_json
      rescue Exception => e
        raise e
        render text: [].to_json
        raise ActiveRecord::Rollback
      end
    end
  end

  def moveSN
    unit = RwUnit.where(serial_number:params[:SN]).first
    
    RwContainer.find(params[:r_container_id]).units.delete(unit)
    RwContainer.find(params[:container_id]).units << unit

    unit.statuses.create status_code: params[:status_code], comments: params[:comment]
    render text: [[{"new_id"=>unit.unit_id}]].to_json
  end


  def getSN
    container = RwContainer.find(params[:container_id])
    render text: container.units.to_json
  end

  def getTest
    process = RwProcess.find(params[:process_id])
    render text: process.tests.to_json
  end
  

private
  after_filter :set_headers
  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'
    headers["Content-Type"] = "application/json"
    true
  end

end
