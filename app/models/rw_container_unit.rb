# == Schema Information
#
# Table name: rw_container_units
#
#  container_id :integer          not null
#  unit_id      :integer          not null
#

class RwContainerUnit < ActiveRecord::Base
  self.table_name = "rw_container_units"
  attr_protected []
  belongs_to :container, class_name: "RwContainer", foreign_key: :container_id
  belongs_to :unit, class_name: "RwUnit", foreign_key: :unit_id
end
