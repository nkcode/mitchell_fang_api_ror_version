# == Schema Information
#
# Table name: rw_containers
#
#  container_id    :integer          not null, primary key
#  container_name  :string(50)       not null
#  container_count :integer
#  user_created_by :integer
#  user_updated_by :integer
#  date_created    :timestamp        not null
#  date_updated    :timestamp        not null
#

class RwContainer < ActiveRecord::Base
  self.table_name = "rw_containers"
  self.primary_key = :container_id
  attr_protected []
  has_many :rw_container_units, foreign_key: "container_id"
  has_many :units, through: :rw_container_units
end
