# == Schema Information
#
# Table name: rw_lot_containers
#
#  lot_id       :integer          not null
#  container_id :integer          not null
#  comments     :string(1000)
#

class RwLotContainer < ActiveRecord::Base
  self.table_name = "rw_lot_containers"
  attr_protected []
  belongs_to :container, class_name: "RwContainer", foreign_key: :container_id
  belongs_to :lot, class_name: "RwLot", foreign_key: :lot_id
end
