# == Schema Information
#
# Table name: rw_process_tests
#
#  test_id     :integer          not null
#  process_id  :integer          not null
#  status_code :string(10)       not null
#

class RwProcessTest < ActiveRecord::Base
  self.table_name = "rw_process_tests"
  attr_protected []
  belongs_to :test, class_name: "RwTest", foreign_key: :test_id
  belongs_to :process, class_name: "RwProcess", foreign_key: :process_id
end
