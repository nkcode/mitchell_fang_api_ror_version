# == Schema Information
#
# Table name: rw_lots
#
#  process_id      :integer          not null
#  lot_id          :integer          not null, primary key
#  lot_name        :string(50)       not null
#  invoice_no      :string(100)
#  input_1         :string(100)
#  input_2         :string(100)
#  input_3         :string(100)
#  user_created_by :integer
#  user_updated_by :integer
#  date_created    :timestamp        not null
#  date_updated    :timestamp        not null
#  lot_type        :string(10)
#

class RwLot < ActiveRecord::Base
  self.table_name = "rw_lots"
  attr_protected [] #make all non protected for auto mass assign
  has_many :rw_lot_containers, foreign_key: :lot_id
  has_many :containers, through: :rw_lot_containers
end
