# == Schema Information
#
# Table name: rw_units
#
#  unit_id         :integer          not null, primary key
#  serial_number   :string(50)       not null
#  user_created_by :integer
#  user_updated_by :integer
#  date_created    :timestamp        not null
#  date_updated    :timestamp        not null
#

class RwUnit < ActiveRecord::Base
  self.table_name = "rw_units"
  self.primary_key = :unit_id
  attr_protected []
  has_many :statuses, class_name: "RwUnitStatus", foreign_key: :unit_id
  has_many :container_units, class_name: "RwContainerUnit", foreign_key: :unit_id
end
