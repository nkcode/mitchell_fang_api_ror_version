# == Schema Information
#
# Table name: rw_unit_statuses
#
#  unit_id         :integer          not null
#  status_code     :string(10)       not null
#  start_status    :timestamp        not null
#  end_status      :timestamp        not null
#  comments        :string(2000)
#  user_created_by :integer
#  user_updated_by :integer
#  date_created    :timestamp        not null
#  date_updated    :timestamp        not null
#

class RwUnitStatus < ActiveRecord::Base
  self.table_name = "rw_unit_statuses"
  attr_protected []
  belongs_to :unit, class_name: "RwUnit", foreign_key: :unit_id
end
