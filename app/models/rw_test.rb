# == Schema Information
#
# Table name: rw_tests
#
#  test_id          :integer          not null, primary key
#  test_name        :string(50)       not null
#  test_instruction :string(1000)     not null
#  user_created_by  :integer
#  user_updated_by  :integer
#  date_created     :timestamp        not null
#  date_updated     :timestamp        not null
#

class RwTest < ActiveRecord::Base
  self.table_name = "rw_tests"
  attr_protected []
end
