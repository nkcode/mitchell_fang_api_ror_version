# == Schema Information
#
# Table name: rw_processes
#
#  process_id      :integer          not null, primary key
#  process_name    :string(50)       not null
#  model_name      :string(100)      not null
#  model_reman_as  :string(100)
#  process_type    :string(50)
#  user_created_by :integer
#  user_updated_by :integer
#  date_created    :timestamp        not null
#  date_updated    :timestamp        not null
#

class RwProcess < ActiveRecord::Base
  self.table_name = "rw_processes"
  attr_protected []
  has_many :rw_process_tests, foreign_key: "process_id"
  has_many :tests, through: :rw_process_tests
end
